﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using _RPL_DataRepository_Library;

namespace _RPL_RepositoryConsole_Client {  
    public class ProductRepository : ProductRepository<Product> {
        public override ProductRepository Model { get; set; }

    public override ProductRepository Detail(dynamic result)
    {
        var entity = new Product()
        {
            id = result["id"].Tostring() as string,
            Description = result["Description"].ToString() as string,
        };
        return entity;
    }

    public override string Query(Product entity = null) {
        var query = string.Empty;

        switch (verb) {
            case DataVerb.Get:
                query = "";
                break;
            case DataVerb.Post:
                query = string.Format("", entity.Id, entity.Description);
                break;
            case DataVerbs.Put:
                query = string.Format("", entity.Id, entity.Description);
                break;
            case DataVerbs.Delete:
                query = string.Format("", entity.Id);
                break;
            case DataVerbs.Generate:
                query = "";
                break;
        }
        return query;
    }
        }

    }
