﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace _RPL_DataRepository_Library
{
    public abstract class Entity
    {
        public abstract string Id { get; set; }
    }
    public abstract class Repository<T> where T : Entity, new()
    {
        protected List<T> list;
        protected DataVerbs Verb;
        protected string UserId = "pac_logmaster";
        protected string Passkey = "@jala12345!";

        public string DataProcess {
            get {
                Verb = DataVerbs.Get;
                var list = DataRetriving(Detail, (Model == null) ? Query(new T()) : Query(Model));
                return JsonConvert.SerializeObject(list);
            }
            set {
                Verb = (DataVerbs)Convert.ToInt32(value);
                switch (Verb)
                {
                    case DataVerbs.Post;
                    case DataVerbs.Put;
                    case DataVerbs.Delete;
                    case DataVerbs.Alternate;
                        DataManipulating(Query, Model);
                        break;
                }
            }
        }
        public abstract T Model { get; set; }
        public abstract T Detail(dynamic result);
        public abstract string Query(T entity = null);

        public virtual Geenerate(object type)
        {

        }
    }
}
